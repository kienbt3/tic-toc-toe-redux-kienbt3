import {combineReducers} from 'redux';
import StatusReducer from "./StatusReducer";

const RootReducer = combineReducers({
  status: StatusReducer,
});

export default RootReducer;
