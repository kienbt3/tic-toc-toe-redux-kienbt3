import StatusConstant from "../constant/StatusConstant";

export default (state = {
  stepNumber: 0,
  xIsNext: true
}, action) => {
  switch (action.type) {
    case StatusConstant.STATUS_SET_NUMBER:
      state = {
        ...state,
        stepNumber: action.payload,
      };
      break;
    case StatusConstant.STATUS_SET_IS_NEXT:
      state = {
        ...state,
        xIsNext: action.payload,
      };
      break;
    default:
      return state;
  }
  return state;
};


