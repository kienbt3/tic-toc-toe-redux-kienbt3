import React from "react";
import Board from "./Board";
import calculateWinner from "../helpers/caculateWinner";
import {connect} from "react-redux";
import StatusAction from "../redux/action/StatusAction";

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            history: [
                {
                    squares: Array(9).fill(null)
                }
            ]
        };
    }

    handleClick(i) {
        const history = this.state.history.slice(0, this.props.status.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        if (calculateWinner(squares) || squares[i]) {
            return;
        }
        squares[i] = this.props.status.xIsNext ? "X" : "O";
        this.props.dispatch(StatusAction.setNumber(history.length));
        this.props.dispatch(StatusAction.setIsNext(!this.props.status.xIsNext));

        this.setState({
            history: history.concat([
                {
                    squares: squares
                }
            ]),
        });
    }

    jumpTo(step) {
        this.props.dispatch(StatusAction.setNumber(step));
        this.props.dispatch(StatusAction.setIsNext((step % 2) === 0));
    }

    render() {
        const history = this.state.history;
        const current = history[this.props.status.stepNumber];
        const winner = calculateWinner(current.squares);

        const moves = history.map((step, move) => {
            const desc = move ?
                'Go to move #' + move :
                'Go to game start';
            return (
                <li key={move}>
                    <button onClick={() => this.jumpTo(move)}>{desc}</button>
                </li>
            );
        });

        let status;
        if (winner) {
            status = "Winner: " + winner;
        } else {
            status = "Next player: " + (this.props.status.xIsNext ? "X" : "O");
        }

        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        squares={current.squares}
                        onClick={i => this.handleClick(i)}
                    />
                </div>
                <div className="game-info">
                    <div>{status}</div>
                    <ol>{moves}</ol>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    let {status} = state;
    return {status};
};

export default connect(mapStateToProps)(Game);

