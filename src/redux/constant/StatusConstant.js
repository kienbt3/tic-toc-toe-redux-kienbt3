const StatusConstant = {
    STATUS_SET_NUMBER : "STATUS_SET_NUMBER",
    STATUS_SET_IS_NEXT : "STATUS_SET_IS_NEXT"
};

export default StatusConstant;
