import React from 'react';
import logo from './logo.svg';
import './App.css';
import Board from "./views/Board";
import Game from "./views/Game";

class App extends React.Component {
    render() {
        return <Game/>
    }
}

export default App;
