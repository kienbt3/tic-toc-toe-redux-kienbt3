import StatusConstant from "../constant/StatusConstant";

const setNumber = (number) => {
    return {
        type: StatusConstant.STATUS_SET_NUMBER,
        payload: number
    }
};

const setIsNext = (status) => {
    return {
        type: StatusConstant.STATUS_SET_IS_NEXT,
        payload: status
    }
};

const StatusAction = {
    setNumber,
    setIsNext
};

export default StatusAction;
